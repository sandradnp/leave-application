package com.example.LeaveApplicationSandra.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.LeaveApplicationSandra.models.BucketApproval;

@Repository
public interface BucketApprovalRepository extends JpaRepository<BucketApproval, Long>{

}
