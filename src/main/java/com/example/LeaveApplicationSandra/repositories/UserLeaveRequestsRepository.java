package com.example.LeaveApplicationSandra.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.LeaveApplicationSandra.models.User;
import com.example.LeaveApplicationSandra.models.UserLeaveRequests;

@Repository
public interface UserLeaveRequestsRepository extends JpaRepository<UserLeaveRequests, Long>{
	//PAGINATION AND SORT
	//namanya harus sesuai variable di kelas user leave request
	public List<UserLeaveRequests> findAllByUser(User user, Pageable pageable);
	
	//UNTUK MENCARI SISA CUTI MINIMAL
	@Query(value = "SELECT MIN(remainder_leave) FROM user_leave_requests WHERE user_id = ?1", nativeQuery = true)
	public Long getMinRemainderLeave(Long user_id); 
	
	//MENCARI TOTAL DATA
	@Query(value = "SELECT COUNT(user_id) FROM user_leave_requests WHERE user_id = ?1", nativeQuery = true)
	public int getTotalData(Long user_id);
	
}
