package com.example.LeaveApplicationSandra.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -636004761993927593L;
    private Object fieldValue;

    public ResourceNotFoundException(Object fieldValue) {
        super(String.format("Permohonan dengan ID %d tidak ditemukan", fieldValue));
        this.fieldValue = fieldValue;
    }

    public Object getFieldValue() {
        return fieldValue;
    }
}
