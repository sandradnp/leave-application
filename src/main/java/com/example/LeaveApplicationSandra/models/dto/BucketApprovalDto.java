package com.example.LeaveApplicationSandra.models.dto;

import java.io.Serializable;
import java.util.Date;

public class BucketApprovalDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6303029292100401204L;
	
	private long bucketApprovalId;
	private UserLeaveRequestsDto userLeaveRequests;
	private String status;
	private Date requestDate;
	private UserDto resolverBy;
	private String resolvedReason;
	private Date resolvedDate;
	private Long createBy;
	private Date createAt;
	private Long updateBy;
	private Date updateAt;
	
	public BucketApprovalDto() {
		
	}

	public BucketApprovalDto(long bucketApprovalId, UserLeaveRequestsDto userLeaveRequests, String status,
			Date requestDate, UserDto resolverBy, String resolvedReason, Date resolvedDate, Long createBy,
			Date createAt, Long updateBy, Date updateAt) {
		super();
		this.bucketApprovalId = bucketApprovalId;
		this.userLeaveRequests = userLeaveRequests;
		this.status = status;
		this.requestDate = requestDate;
		this.resolverBy = resolverBy;
		this.resolvedReason = resolvedReason;
		this.resolvedDate = resolvedDate;
		this.createBy = createBy;
		this.createAt = createAt;
		this.updateBy = updateBy;
		this.updateAt = updateAt;
	}

	public long getBucketApprovalId() {
		return bucketApprovalId;
	}

	public void setBucketApprovalId(long bucketApprovalId) {
		this.bucketApprovalId = bucketApprovalId;
	}

	public UserLeaveRequestsDto getUserLeaveRequests() {
		return userLeaveRequests;
	}

	public void setUserLeaveRequests(UserLeaveRequestsDto userLeaveRequests) {
		this.userLeaveRequests = userLeaveRequests;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public UserDto getResolverBy() {
		return resolverBy;
	}

	public void setResolverBy(UserDto resolverBy) {
		this.resolverBy = resolverBy;
	}

	public String getResolvedReason() {
		return resolvedReason;
	}

	public void setResolvedReason(String resolvedReason) {
		this.resolvedReason = resolvedReason;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public Long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Long createBy) {
		this.createBy = createBy;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Long getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Long updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	
}
