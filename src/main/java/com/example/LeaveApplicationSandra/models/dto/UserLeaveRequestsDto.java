package com.example.LeaveApplicationSandra.models.dto;

import java.io.Serializable;
import java.util.Date;

public class UserLeaveRequestsDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3235972334279127861L;
	
	private long leaveRequestId;
	private UserDto user;
	private Date requestDate;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private String description;
	private String status;
	private String resolverReason;
	private Long resolvedBy;
	private Date resolvedDate;
	private long remainderLeave;
	private long createBy;
	private Date createAt;
	private Long updateBy;
	private Date updateAt;
	
	public UserLeaveRequestsDto() {
		
	}

	public UserLeaveRequestsDto(long leaveRequestId, UserDto user, Date requestDate, Date leaveDateFrom,
			Date leaveDateTo, String description, String status, String resolverReason, Long resolvedBy,
			Date resolvedDate, long remainderLeave, long createBy, Date createAt, Long updateBy, Date updateAt) {
		super();
		this.leaveRequestId = leaveRequestId;
		this.user = user;
		this.requestDate = requestDate;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.description = description;
		this.status = status;
		this.resolverReason = resolverReason;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
		this.remainderLeave = remainderLeave;
		this.createBy = createBy;
		this.createAt = createAt;
		this.updateBy = updateBy;
		this.updateAt = updateAt;
	}

	public long getLeaveRequestId() {
		return leaveRequestId;
	}

	public void setLeaveRequestId(long leaveRequestId) {
		this.leaveRequestId = leaveRequestId;
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	public Date getLeaveDateTo() {
		return leaveDateTo;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResolverReason() {
		return resolverReason;
	}

	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}

	public Long getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(Long resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public long getRemainderLeave() {
		return remainderLeave;
	}

	public void setRemainderLeave(long remainderLeave) {
		this.remainderLeave = remainderLeave;
	}

	public long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(long createBy) {
		this.createBy = createBy;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Long getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Long updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}
	
	
}
