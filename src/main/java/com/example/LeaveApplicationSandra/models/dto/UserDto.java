package com.example.LeaveApplicationSandra.models.dto;

import java.io.Serializable;
import java.util.Date;

public class UserDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2733901745419498634L;
	
	private long userId;
	private PositionDto userPosition;
	private String userName;
	private String userGender;
	private String userAddress;
	private String userPhone;
	private long createBy;
	private Date createAt;
	private Long updateBy;
	private Date updateAt;
	
	public UserDto() {
		
	}

	public UserDto(long userId, PositionDto userPosition, String userName, String userGender, String userAddress,
			String userPhone, long createBy, Date createAt, Long updateBy, Date updateAt) {
		super();
		this.userId = userId;
		this.userPosition = userPosition;
		this.userName = userName;
		this.userGender = userGender;
		this.userAddress = userAddress;
		this.userPhone = userPhone;
		this.createBy = createBy;
		this.createAt = createAt;
		this.updateBy = updateBy;
		this.updateAt = updateAt;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public PositionDto getUserPosition() {
		return userPosition;
	}

	public void setUserPosition(PositionDto userPosition) {
		this.userPosition = userPosition;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserGender() {
		return userGender;
	}

	public void setUserGender(String userGender) {
		this.userGender = userGender;
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(long createBy) {
		this.createBy = createBy;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Long getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Long updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	
}