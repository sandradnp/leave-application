package com.example.LeaveApplicationSandra.models.dto;

import java.io.Serializable;
import java.util.Date;

public class PositionLeaveDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5555345505649088913L;
	
	private long positionLeaveId;
	private Long positionLeaveLimit;
	private long createBy;
	private Date createAt;
	private Long updateBy;
	private Date updateAt;
	
	public PositionLeaveDto() {
		
	}

	public PositionLeaveDto(long positionLeaveId, Long positionLeaveLimit, long createBy, Date createAt, Long updateBy,
			Date updateAt) {
		super();
		this.positionLeaveId = positionLeaveId;
		this.positionLeaveLimit = positionLeaveLimit;
		this.createBy = createBy;
		this.createAt = createAt;
		this.updateBy = updateBy;
		this.updateAt = updateAt;
	}

	public long getPositionLeaveId() {
		return positionLeaveId;
	}

	public void setPositionLeaveId(long positionLeaveId) {
		this.positionLeaveId = positionLeaveId;
	}

	public Long getPositionLeaveLimit() {
		return positionLeaveLimit;
	}

	public void setPositionLeaveLimit(Long positionLeaveLimit) {
		this.positionLeaveLimit = positionLeaveLimit;
	}

	public long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(long createBy) {
		this.createBy = createBy;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Long getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Long updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	
}
