package com.example.LeaveApplicationSandra.models.dto;

import java.io.Serializable;
import java.util.Date;

public class PositionDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3981417759126172305L;
	
	private long positionId;
	private String postionTitle;
	private long createBy;
	private Date createAt;
	private Long updateBy;
	private Date updateAt;
	private PositionLeaveDto positionLeave;
	
	public PositionDto() {
		
	}

	public PositionDto(long positionId, String postionTitle, long createBy, Date createAt, Long updateBy, Date updateAt,
			PositionLeaveDto positionLeave) {
		super();
		this.positionId = positionId;
		this.postionTitle = postionTitle;
		this.createBy = createBy;
		this.createAt = createAt;
		this.updateBy = updateBy;
		this.updateAt = updateAt;
		this.positionLeave = positionLeave;
	}

	public long getPositionId() {
		return positionId;
	}

	public void setPositionId(long positionId) {
		this.positionId = positionId;
	}

	public String getPostionTitle() {
		return postionTitle;
	}

	public void setPostionTitle(String postionTitle) {
		this.postionTitle = postionTitle;
	}

	public long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(long createBy) {
		this.createBy = createBy;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Long getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Long updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	public PositionLeaveDto getPositionLeave() {
		return positionLeave;
	}

	public void setPositionLeave(PositionLeaveDto positionLeave) {
		this.positionLeave = positionLeave;
	}

	
}
