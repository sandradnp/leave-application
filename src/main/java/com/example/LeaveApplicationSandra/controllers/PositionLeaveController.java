package com.example.LeaveApplicationSandra.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.LeaveApplicationSandra.models.PositionLeave;
import com.example.LeaveApplicationSandra.models.dto.PositionLeaveDto;
import com.example.LeaveApplicationSandra.repositories.PositionLeaveRepository;
import com.example.LeaveApplicationSandra.repositories.PositionRepository;

@RestController
@RequestMapping("/api/positionLeave")
public class PositionLeaveController {
	@Autowired
	PositionLeaveRepository positionLeaveRepo;
	@Autowired
	PositionRepository positionRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//CREATE
	@PostMapping("/create")
	public Map<String, Object> createPositionLeave(@Valid @RequestBody PositionLeaveDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		PositionLeave positionLeave = modelMapper.map(body, PositionLeave.class);
		positionLeaveRepo.save(positionLeave);
		body.setPositionLeaveId(positionLeave.getPositionLeaveId());
		
		result.put("message", "create success");
		result.put("data", body);
		return result;
	}
	
	//READ ALL
	@GetMapping("readAll")
	public Map<String, Object> readAllPositionLeave() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<PositionLeaveDto> listPositionLeaveDto = new ArrayList<PositionLeaveDto>();
		for(PositionLeave positionLeave:positionLeaveRepo.findAll()) {
			PositionLeaveDto positionLeaveDto = new PositionLeaveDto();
			positionLeaveDto = modelMapper.map(positionLeave, PositionLeaveDto.class);
			listPositionLeaveDto.add(positionLeaveDto);
		}
		result.put("message", "read all success");
		result.put("data", listPositionLeaveDto);
		return result;
	}
	
	//READ BY ID
	@GetMapping("readById")
	public Map<String, Object> readByIdPositionLeave(@RequestParam("id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		PositionLeave positionLeave = positionLeaveRepo.findById(id).get();
		PositionLeaveDto positionLeaveDto = modelMapper.map(positionLeave, PositionLeaveDto.class);
		result.put("message", "read by id success");
		result.put("data", positionLeaveDto);
		return result;
	}
	
	//UPDATE
	@PutMapping("update")
	public Map<String, Object> readByIdPositionLeave(@RequestParam("id") Long id, @Valid @RequestBody PositionLeaveDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		for(PositionLeave positionLeave:positionLeaveRepo.findAll()) {
			if(positionLeave.getPositionLeaveId()==id) {			
				positionLeave.setPositionLeaveLimit(body.getPositionLeaveLimit());
				positionLeave.setUpdateBy(body.getUpdateBy());
				positionLeave.setUpdateAt(body.getUpdateAt());
				body.setPositionLeaveId(positionLeave.getPositionLeaveId());
				
				positionLeaveRepo.save(positionLeave);
			}
		}
		result.put("message", "update success");
		result.put("data", body);
		return result;
	}
	
	//DELETE
	@DeleteMapping("delete")
	public Map<String, Object> deletePositionLeave(@RequestParam("id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		positionLeaveRepo.deleteById(id);
		result.put("message", "delete success");
		return result;
	}
}
