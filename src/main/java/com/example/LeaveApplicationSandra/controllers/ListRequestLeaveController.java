package com.example.LeaveApplicationSandra.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.LeaveApplicationSandra.models.User;
import com.example.LeaveApplicationSandra.models.UserLeaveRequests;
import com.example.LeaveApplicationSandra.models.dto.UserDto;
import com.example.LeaveApplicationSandra.models.dto.UserLeaveRequestsDto;
import com.example.LeaveApplicationSandra.repositories.UserLeaveRequestsRepository;
import com.example.LeaveApplicationSandra.repositories.UserRepository;

@RestController
@RequestMapping("/api/")
public class ListRequestLeaveController {
	@Autowired
	UserLeaveRequestsRepository leaveRequestsRepo;
	@Autowired
	UserRepository userRepo;
	
	//NO 3
	@GetMapping("/listRequestLeave/{userId}/{totalDataPerPage}/{choosenPage}")
	public Map<String, Object> getListRequestLeave (@PathVariable(value = "userId") Long userId, @PathVariable(value = "totalDataPerPage") int totalDataPerPage, @PathVariable(value = "choosenPage") int choseenPage) {
		Map<String, Object> result = new HashMap<String, Object>();
		User user = userRepo.findById(userId).get();
		Pageable sortedById = PageRequest.of(choseenPage, totalDataPerPage, Sort.by("leaveRequestId"));
		List<UserLeaveRequests> listRequest = leaveRequestsRepo.findAllByUser(user, sortedById);
		List<UserLeaveRequestsDto> listRequestDto = new ArrayList<UserLeaveRequestsDto>();
		for(UserLeaveRequests request:listRequest) {
			UserLeaveRequestsDto requestDto = new UserLeaveRequestsDto();
			UserDto userDto = new UserDto();
			//SAYA HANYA MENAMPILKAN DATA YANG DIMINTA DI SOAL
			userDto.setUserId(request.getUser().getUserId());
			requestDto.setUser(userDto);
			requestDto.setLeaveDateFrom(request.getLeaveDateFrom());
			requestDto.setLeaveDateTo(request.getLeaveDateTo());
			requestDto.setDescription(request.getDescription());
			requestDto.setStatus(request.getStatus());
			requestDto.setResolverReason(request.getResolverReason());
			requestDto.setResolvedBy(request.getResolvedBy());
			requestDto.setResolvedDate(request.getResolvedDate());
			
			listRequestDto.add(requestDto);
		}
		result.put("total data: ", leaveRequestsRepo.getTotalData(userId));
		result.put("read page: ", choseenPage);
		result.put("total items in this page: ", listRequest.size());
		result.put("items: ", listRequestDto);
		return result;
	}
}
