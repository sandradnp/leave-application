package com.example.LeaveApplicationSandra.controllers;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.LeaveApplicationSandra.exceptions.ResourceNotFoundException;
import com.example.LeaveApplicationSandra.models.BucketApproval;
import com.example.LeaveApplicationSandra.models.User;
import com.example.LeaveApplicationSandra.models.UserLeaveRequests;
import com.example.LeaveApplicationSandra.models.dto.BucketApprovalDto;
import com.example.LeaveApplicationSandra.repositories.BucketApprovalRepository;
import com.example.LeaveApplicationSandra.repositories.UserLeaveRequestsRepository;
import com.example.LeaveApplicationSandra.repositories.UserRepository;

@RestController
@RequestMapping("/api/resolveRequestLeave")
public class ResolvedRequestLeaveController {
	@Autowired
	BucketApprovalRepository bucketApprovalRepo;
	@Autowired
	UserLeaveRequestsRepository userRequestRepo;
	@Autowired
	UserRepository userRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//NO 4
	@PostMapping
	public Map<String, Object> postBucketApproval(@Valid @RequestBody BucketApprovalDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		String message = validateResponse(body);
		
		result.put("message", message);
		return result;
	}
	
	//VALIDASI RESPON
	public String validateResponse(BucketApprovalDto body) {
		String message;
		UserLeaveRequests userRequest = userRequestRepo.findById(body.getUserLeaveRequests().getLeaveRequestId()).orElseThrow(() -> new ResourceNotFoundException(body.getUserLeaveRequests().getLeaveRequestId()));
		if(body.getResolvedDate().compareTo(userRequest.getRequestDate()) < 0) {
			message = "Kesalahan data, tanggal keputusan tidak bisa lebih awal dari pengajuan cuti.";
		} else {
			BucketApproval bucketApproval = modelMapper.map(body, BucketApproval.class);
			message = "Anda tidak berhak mengambil keputusan dari Permohonan dengan ID "+body.getUserLeaveRequests().getLeaveRequestId();
			if (body.getStatus().equalsIgnoreCase("approved")) {
				if (validateResponder(userRequest, bucketApproval)) { //VALIDASI PEMBERI RESPON
					message = "Permohonan dengan ID "+body.getUserLeaveRequests().getLeaveRequestId()+" telah berhasil diputuskan.";
					bucketApprovalRepo.save(bucketApproval);
					calculateRemaindingLeave(userRequest, bucketApproval);
				}
			}
		}
		return message;
	}
	
	//METHOD VALIDASI PEMBERI RESPON
	public boolean validateResponder(UserLeaveRequests userRequest, BucketApproval bucketApproval) {
		boolean isAllowed = false;
		User user = userRepo.findById(bucketApproval.getResolverBy().getUserId()).get();
		if(userRequest.getUser().getUserPosition().getPostionTitle().equalsIgnoreCase("employee") && user.getUserPosition().getPostionTitle().equalsIgnoreCase("supervisor")) {
			isAllowed = true;
		} else if(userRequest.getUser().getUserPosition().getPostionTitle().equalsIgnoreCase("supervisor") && user.getUserPosition().getPostionTitle().equalsIgnoreCase("supervisor")) {
			if(userRequest.getUser().getUserId() != bucketApproval.getResolverBy().getUserId()) {
				isAllowed = true;
			}
		} else if(userRequest.getUser().getUserPosition().getPostionTitle().equalsIgnoreCase("staff") && user.getUserPosition().getPostionTitle().equalsIgnoreCase("staff")) {
			isAllowed = true;
		}
		return isAllowed;
	}
	
	//JIKA RESPON DISETUJUI MAKA MENGHITUNG SISA GAJI & MENGISI KOLOM PADA USER_LEAVE_REQUEST
	public void calculateRemaindingLeave(UserLeaveRequests userRequest, BucketApproval bucketApproval) {
		java.util.Date leaveFromDate = new java.util.Date(userRequest.getLeaveDateFrom().getTime());
		java.util.Date leaveToDate = new java.util.Date(userRequest.getLeaveDateTo().getTime());
		LocalDate leaveFrom = leaveFromDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate leaveTo = leaveToDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		long leaveDays = ChronoUnit.DAYS.between(leaveFrom, leaveTo);
		
		userRequest.setRemainderLeave(userRequest.getRemainderLeave()-leaveDays);
		userRequest.setStatus(bucketApproval.getStatus());
		userRequest.setResolvedBy(bucketApproval.getResolverBy().getUserId());
		userRequest.setResolvedDate(bucketApproval.getResolvedDate());
		userRequest.setResolverReason(bucketApproval.getResolvedReason());
		userRequestRepo.save(userRequest);
	}
}
