package com.example.LeaveApplicationSandra.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.LeaveApplicationSandra.models.User;
import com.example.LeaveApplicationSandra.models.dto.UserDto;
import com.example.LeaveApplicationSandra.repositories.UserRepository;

@RestController
@RequestMapping("/api/user")
public class UserController {
	@Autowired
	UserRepository userRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//CREATE
	@PostMapping("/create")
	public Map<String, Object> createUser(@Valid @RequestBody UserDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		User user = modelMapper.map(body, User.class);
		userRepo.save(user);
		body.setUserId(user.getUserId());
		
		result.put("message", "create success");
		result.put("data", body);
		return result;
	}
	
	//READ ALL
	@GetMapping("readAll")
	public Map<String, Object> readAllUser() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<UserDto> listUserDto = new ArrayList<UserDto>();
		for(User user:userRepo.findAll()) {
			UserDto userDto = new UserDto();
			userDto = modelMapper.map(user, UserDto.class);
			listUserDto.add(userDto);
		}
		result.put("message", "read all success");
		result.put("data", listUserDto);
		return result;
	}
	
	//READ BY ID
	@GetMapping("readById")
	public Map<String, Object> readByIdUser(@RequestParam("id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		User user = userRepo.findById(id).get();
		UserDto userDto = modelMapper.map(user, UserDto.class);
		result.put("message", "read by id success");
		result.put("data", userDto);
		return result;
	}
	
	//UPDATE
	@PutMapping("update")
	public Map<String, Object> readByIdUser(@RequestParam("id") Long id, @Valid @RequestBody UserDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		for(User user:userRepo.findAll()) {
			if(user.getUserId()==id) {
				body.setUserId(id);
				body.setCreateAt(user.getCreateAt());
				body.setCreateBy(user.getCreateBy());
				user = modelMapper.map(body, User.class);
				userRepo.save(user);
			}
		}
		result.put("message", "update success");
		result.put("data", body);
		return result;
	}
		
	//DELETE
	@DeleteMapping("delete")
	public Map<String, Object> deleteUser(@RequestParam("id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		userRepo.deleteById(id);
		result.put("message", "delete success");
		return result;
	}
}
