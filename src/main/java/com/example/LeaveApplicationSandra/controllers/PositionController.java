package com.example.LeaveApplicationSandra.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.LeaveApplicationSandra.models.Position;
import com.example.LeaveApplicationSandra.models.PositionLeave;
import com.example.LeaveApplicationSandra.models.dto.PositionDto;
import com.example.LeaveApplicationSandra.repositories.PositionRepository;

@RestController
@RequestMapping("/api/position")
public class PositionController {
	@Autowired
	PositionRepository positionRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//CREATE
	@PostMapping("/create")
	public Map<String, Object> createPosition(@Valid @RequestBody PositionDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		Position position = modelMapper.map(body, Position.class);
		positionRepo.save(position);
		body.setPositionId(position.getPositionId());
		
		result.put("message", "create success");
		result.put("data", body);
		return result;
	}
	
	//READ ALL
	@GetMapping("readAll")
	public Map<String, Object> readAllPosition() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<PositionDto> listPositionDto = new ArrayList<PositionDto>();
		for(Position position:positionRepo.findAll()) {
			PositionDto positionDto = new PositionDto();
			positionDto = modelMapper.map(position, PositionDto.class);
			listPositionDto.add(positionDto);
		}
		result.put("message", "read all success");
		result.put("data", listPositionDto);
		return result;
	}
	
	//READ BY ID
	@GetMapping("readById")
	public Map<String, Object> readByIdPosition(@RequestParam("id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Position position = positionRepo.findById(id).get();
		PositionDto positionDto = modelMapper.map(position, PositionDto.class);
		result.put("message", "read by id success");
		result.put("data", positionDto);
		return result;
	}
	
	//UPDATE
	@PutMapping("update")
	public Map<String, Object> readByIdPosition(@RequestParam("id") Long id, @Valid @RequestBody PositionDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		for(Position position:positionRepo.findAll()) {
			if(position.getPositionId()==id) {
				PositionLeave positionLeave = new PositionLeave();
				positionLeave.setPositionLeaveId(body.getPositionLeave().getPositionLeaveId());
				position.setPositionLeave(positionLeave);
				position.setPostionTitle(body.getPostionTitle());
				position.setUpdateBy(body.getUpdateBy());
				body.setPositionId(position.getPositionId());
				positionRepo.save(position);
			}
		}
		result.put("message", "update success");
		result.put("data", body);
		return result;
	}
	
	//DELETE
	@DeleteMapping("delete")
	public Map<String, Object> deletePosition(@RequestParam("id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		positionRepo.deleteById(id);
		result.put("message", "delete success");
		return result;
	}
}
