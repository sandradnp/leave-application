package com.example.LeaveApplicationSandra.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.LeaveApplicationSandra.models.User;
import com.example.LeaveApplicationSandra.models.UserLeaveRequests;
import com.example.LeaveApplicationSandra.models.dto.UserLeaveRequestsDto;
import com.example.LeaveApplicationSandra.repositories.UserLeaveRequestsRepository;
import com.example.LeaveApplicationSandra.repositories.UserRepository;

@RestController
@RequestMapping("/api/requestLeave")
public class RequestLeaveController {
	@Autowired
	UserLeaveRequestsRepository leaveRequestRepo;
	@Autowired
	UserRepository userRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//NO. 2
	@PostMapping
	public Map<String, Object> postLeaveRequest(@Valid @RequestBody UserLeaveRequestsDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		String message = validateRequest(body);
		
		result.put("message", message);
		return result;
	}
	
	//VALIDASI REQUEST DARI USER, SUDAH MEMENUHI SYARAT ATAU TIDAK
	public String validateRequest(UserLeaveRequestsDto body) {
		Date date = new Date(System.currentTimeMillis());
		String message;
		Long reminderLeave = getReminderLeave(body);
		long leaveDays = calculateLeaveDays(body);
		java.sql.Date fromDate = new java.sql.Date(body.getLeaveDateFrom().getTime());
		java.sql.Date toDate = new java.sql.Date(body.getLeaveDateTo().getTime());
		if(reminderLeave==0) {
			message = "Mohon maaf, jatah cuti Anda telah habis.";
		} else if(reminderLeave < leaveDays) {
			message = "Mohon maaf, jatah cuti Anda tidak cukup untuk digunakan dari tanggal "+fromDate+" sampai "+toDate+" ("+leaveDays+" hari). Jatah cuti Anda yang tersisa adalah "+reminderLeave+" hari.";
		} else if(body.getLeaveDateFrom().compareTo(body.getLeaveDateTo()) > 0) {
			message = "Tanggal yang Anda ajukan tidak valid.";
		} else if (body.getLeaveDateFrom().compareTo(date) < 0) {
			message = "Tanggal yang Anda ajukan telah lampau, silahkan ganti tanggal pengajuan cuti anda.";
		} else {
			UserLeaveRequests leaveRequest = modelMapper.map(body, UserLeaveRequests.class);
			leaveRequest.setRemainderLeave(reminderLeave);
			leaveRequest.setStatus("waiting..");
			leaveRequest.setRequestDate(date);
			leaveRequestRepo.save(leaveRequest);
			message = "Permohonan anda sedang di proses.";
		}
		return message;
	}
	
	//MENCARI SISA CUTI DARI USER
	public long getReminderLeave(UserLeaveRequestsDto body) {
		Date date = new Date(System.currentTimeMillis());
		Calendar currentYear = Calendar.getInstance();
		currentYear.setTime(date);
		
		//MENCARI DATA USER PERNAH MENGAMBIL CUTI ATAU TIDAK
		User user = userRepo.findById(body.getUser().getUserId()).get();
		Long reminderLeave = user.getUserPosition().getPositionLeave().getPositionLeaveLimit();
		if(user.getUserLeaveRequestses().size()!=0) {	
			for(UserLeaveRequests request:user.getUserLeaveRequestses()) {
				Calendar year = Calendar.getInstance();
				year.setTime(request.getLeaveDateFrom());
				//JIKA PERNAH MENGAMBIL CUTI PADA TAHUN SEKARANG, AMBIL SISA CUTI
				if(year.get(Calendar.YEAR)==currentYear.get(Calendar.YEAR)) {
					reminderLeave = leaveRequestRepo.getMinRemainderLeave(user.getUserId());	
				}
			}
		}
		return reminderLeave;
	}
	
	//MENGHITUNG LAMA CUTI
	public long calculateLeaveDays(UserLeaveRequestsDto body) {
		LocalDate leaveFrom = body.getLeaveDateFrom().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate leaveTo = body.getLeaveDateTo().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		long leaveDays = ChronoUnit.DAYS.between(leaveFrom, leaveTo);
		return leaveDays;
	}
}
