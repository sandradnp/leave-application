package com.example.LeaveApplicationSandra.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.LeaveApplicationSandra.models.User;
import com.example.LeaveApplicationSandra.models.UserLeaveRequests;
import com.example.LeaveApplicationSandra.repositories.UserLeaveRequestsRepository;
import com.example.LeaveApplicationSandra.repositories.UserRepository;

@RestController
@RequestMapping("/api/annualLeaveReport")
public class AnnualLeaveReportController {
	@Autowired
	UserLeaveRequestsRepository userRequestRepo;
	@Autowired
	UserRepository userRepo;
	
	//NO 5 MENGAMBIL DATA UNTUK LAPORAN PENGAMBILAN CUTI
	@GetMapping
	public Map<Integer, Object> getAnnualLeaveReport(@RequestParam("year")@DateTimeFormat(pattern = "yyyy") Date year) {
		Map<Integer, Object> allData = new HashMap<Integer, Object>();
		Calendar yearInput = Calendar.getInstance();
		yearInput.setTime(year);
		List<UserLeaveRequests> listRequests = userRequestRepo.findAll();
		int index = 0;
		for(UserLeaveRequests request:listRequests) {
			Calendar yearData = Calendar.getInstance();
			yearData.setTime(request.getLeaveDateFrom());
			if(yearData.get(Calendar.YEAR)==yearInput.get(Calendar.YEAR) && !request.getStatus().equalsIgnoreCase("waiting..")) {
				Map<String, Object> data = new HashMap<String, Object>();
				User user = userRepo.findById(request.getResolvedBy()).get();
				data.put("id: ", request.getUser().getUserId());
				data.put("nama: ", request.getUser().getUserName());
				data.put("posisi: ", request.getUser().getUserPosition().getPostionTitle());
				data.put("tanggal mulai cuti: ", request.getLeaveDateFrom());
				data.put("tanggal selesai cuti: ", request.getLeaveDateTo());
				data.put("sisa cuti: ", request.getRemainderLeave());
				data.put("deskripsi: ", request.getDescription());
				data.put("status: ", request.getStatus());
				data.put("alasan keputusan: ", request.getResolverReason());
				data.put("pengambil keputusan: ", user.getUserName());
				index++;
				allData.put(index, data);
			}
		}
		
		return allData;
	}
}
