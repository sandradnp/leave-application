package com.example.LeaveApplicationSandra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeaveApplicationSandraApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeaveApplicationSandraApplication.class, args);
	}

}
